package ru.tsc.gavran.tm.exception.system;

import ru.tsc.gavran.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error. This value`" + value + "` is not number");
    }

}
