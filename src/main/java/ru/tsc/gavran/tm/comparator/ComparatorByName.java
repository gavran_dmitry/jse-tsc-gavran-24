package ru.tsc.gavran.tm.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.entity.IHasName;

import java.util.Comparator;

public final class ComparatorByName implements Comparator<IHasName> {

    @NotNull
    private static final ComparatorByName INSTANCE = new ComparatorByName();

    private ComparatorByName() {
    }

    public static ComparatorByName getInstance() {
        return INSTANCE;
    }

    @NotNull
    @Override
    public int compare(@Nullable IHasName o1, @Nullable IHasName o2) {
        if (o1 == null || o2 == null)
            return 0;
        return o1.getName().compareTo(o2.getName());
    }

}