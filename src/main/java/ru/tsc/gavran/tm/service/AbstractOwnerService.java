package ru.tsc.gavran.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IOwnerRepository;
import ru.tsc.gavran.tm.api.service.IOwnerService;
import ru.tsc.gavran.tm.exception.empty.EmptyIdException;
import ru.tsc.gavran.tm.exception.empty.EmptyIndexException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    @NotNull
    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Nullable
    @Override
    public E add(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return null;
        return repository.add(userId, entity);
    }

    @Override
    public void remove(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) throw new ProcessException();
        repository.remove(userId, entity);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findAll(userId);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        repository.clear(userId);
    }

    @Nullable
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Nullable
    @Override
    public E removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (comparator == null) return null;
        return repository.findAll(userId, comparator);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    public E removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(userId, index);
    }

}