package ru.tsc.gavran.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractCommand;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login user to system.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        System.out.println("Welcome to hell, " + user.getFirstName() + " " + user.getMiddleName() + " !");
    }

}