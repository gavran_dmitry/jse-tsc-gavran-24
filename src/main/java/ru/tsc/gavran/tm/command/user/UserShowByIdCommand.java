package ru.tsc.gavran.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class UserShowByIdCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findById(id);
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}