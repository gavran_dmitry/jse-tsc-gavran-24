package ru.tsc.gavran.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractTaskCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.List;

public class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list-by-project-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Display task list by project id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(userId, projectId);
        if (tasks == null) throw new TaskNotFoundException();
        int index = 0;
        for (Task task : tasks) {
            index++;
            System.out.println(index + ". " + task.toString());
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}