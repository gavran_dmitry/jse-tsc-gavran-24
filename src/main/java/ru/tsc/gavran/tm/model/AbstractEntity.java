package ru.tsc.gavran.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public abstract class AbstractEntity {

    @Getter
    @Setter
    @NotNull
    protected String id = UUID.randomUUID().toString();

}
